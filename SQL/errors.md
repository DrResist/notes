# Raiserror statements

* RAISERROR. Syntax {severity, state}

```sql
IF NOT EXISTS (SELECT * FROM staff WHERE staff_id = 15)
    RAISERROR('NO staff member with such id',16,1)

IF NOT EXISTS (SELECT * FROM staff WHERE staff_id = 15)
    RAISERROR('No %s with id %d.', 16, 1, 'staff member', 15);

-- With try | catch

BEGIN TRY
    IF NOT EXISTS (SELECT * FROM staff WHERE staff_id = 15)
        RAISERROR('No %s with id %d.', 16, 1, 'staff member', 15);
END TRY
BEGIN CATCH
    SELECT 'You are in the CATCH block' AS message
END CATCH
```

## Throw statement

* Recommend over RAISERROR
* Syntax `THROW [error_number, message, state][:]`
  
```sql

-- Throw must be first and with ;
BEGIN TRY
    SELECT price/0 FROM orders;
END TRY
BEGIN CATCH;
    THROW;
    SELECT 'This line is executed!' AS message;
END CATCH

-- With params

THROW 52000, 'This is an example', 1;


BEGIN TRY
    IF NOT EXISTS (SELECT * FROM staff WHERE staff_id = 15)
        THROW 51000, 'This is an example', 1;
END TRY
BEGIN CATCH
    SELECT ERROR_MESSAGE() AS message;
END CATCH
```

*Remember that the THROW statement without parameters re-throws the original error.*


```sql
CREATE PROCEDURE insert_product
  @product_name VARCHAR(50),
  @stock INT,
  @price DECIMAL

AS

BEGIN TRY
	INSERT INTO products (product_name, stock, price)
		VALUES (@product_name, @stock, @price);
END TRY
-- Set up the CATCH block
BEGIN CATCH
	-- Insert the error and end the statement with a semicolon
    INSERT INTO errors VALUES ('Error inserting a product');
    -- Re-throw the error
	THROW; 
END CATCH

BEGIN TRY
	-- Execute the stored procedure
	EXEC insert_product
    	-- Set the values for the parameters
    	@product_name = 'Super bike',
        @stock = 3,
        @price = 499.99;
END TRY
-- Set up the CATCH block
BEGIN CATCH
	-- Select the error message
	SELECT ERROR_MESSAGE() as message;
END CATCH
```

## Customize error message in THROW

Ways of customizing error message:

* Using a variable and the CONCAT function

```sql
DECLARE @staff_id AS INT = 500;
DECLARE @my_message NVARCHAR(500) = 
    CONCAT('There is no staff member for id', @staff_id, '. Try with another one.');
IF NOT EXISTS (SELECT * FROM staff WHERE staff_id = @staff_id)
    THROW 50000, @my_message, 1;
```

* Using FORMATMESSAGE function

```sql
DECLARE @staff_id AS INT = 500;
DECLARE @my_message NVARCHAR(500) = 
    FORMATMESSAGE('There is no staff member for id %d. %s', @staff_id, 'Try with another one.');
IF NOT EXISTS (SELECT * FROM staff WHERE staff_id = @staff_id)
    THROW 50000, @my_message, 1;
```

FORMATMESSAGE with message number

```SQL
sp_addmessage
    msg_id, severity, msgtext,
    [language],
    [with_log {'TRUE'|'FALSE'}],
    [replace]
--msg_id >50000

EXEC sp_addmessage
    @msgnum = 55000, @severity = 16, @msgtext = 'There is no staff member for id %d. %s', @lang = N'us_english';
```

`SELECT * FROM sys.message`

