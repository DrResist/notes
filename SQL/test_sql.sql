SELECT
	v.name AS venue,
	c.name AS city
FROM venue as v
INNER JOIN city as c
	ON v.city_id = c.id
LIMIT 3;