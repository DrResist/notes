# DB Desing 


## OLTP and OLAP 

* Schemas - how should data be logically organized
* Normalization - should my data have minimal dependencies and redundancy
* Views - what joins will be done mostly
* Access control - should all users of the data have same access
* DBMS - how pick between all the SQL and noSQL options


* OLTP online transactions processing
	* TASKS: 
		* find price of a book
		* update last customer transactions
		* keep track of employee hours


* OLAP online analytical processing
	* TASKS: 
		* calculate books with bets profit margin
		* find more loayl customers

||OLTP|OLAP|
|-|-|-|
Purpose|support daily transactions|report and analyze data
Design| app-oriented|subject-oriented
Data | up-to-date, operational|consolidated, historical
Size | snapshot, gigabytes|archive, terbytes
Queries|simple transactions|complex, agg queries & limit updates
Users|thousands|hundreed

## Storing data

1. Structed data
	* Follows a schema
	* Defined data types & relationships
2. Unstructured data
	* Schemaless
	* Makes up most of data in the world
3. Semi-structured data
	* Does not follow larger schema
	* Self-describing structure
	* *NoSQL,XML,JSON*

### Storing data beyond traditional db

* Traditional db
	* For storing real-time struct data **OLTP**
* DWH
	* For analyzing archived structured data **OLAP**
* Data lakes
	* For storing data of all struct. = flexibility and scalability
	* For analyzing Big Data

## ETL ELT 

* ETL 
	* Data Sources (OLTP, API, Files) - Extract - Transform (staging) - Load - DWH - Use(ML, BI tools, Analytics)

* ELT 
	* Data Sources - Extract - (LOAD and Transform in Data Lake) - Use


## Data modelling

1. Conceptual data model. Describe entities, relations, attributes
	* Tools: data structure diagrams, ER diagrams, UML diagrams
2. Logical data model: Define tables, columns, relations
	* Tools: DB models and schemas, relational model and star schema
3. Physical data model: describe physical storage
	* Tools: Partitions, CPUs, indexes, backup systems, tablespaces