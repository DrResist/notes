# Main

*Мой мини-cheatsheet по Spark*

## Loading data in PySpark shell
```python
# Load a local file into PySpark shell
lines = sc.textFile(file_path)
```

## Abstracting Data with RDDs

* RDD - resilient distributed datasets

*Decomposing RDDs*
* Resilient Distributed Datasets
    * Resilient: Ability to whitstand failures
    * Distributed: Spanning across multiple machines
    * Datasets: Collection of partitioned data e.g, Arrays, Tables, Tuples, etc.   

## Creating RDDs
* Parallelizing an existing collection of objects
* External datasets:
    * Files in HDFS
    * Objects in Amazon S3 bucket
    * lines in a text file
* From existing RDDs

*Parallelized collection*

* `parallelize()` for creating RDD from python list
```python
numRDD = sc.parallelize([1,2,3,4])
```
*From external datasets*
* `textFile()` creating RDD from external datasets

## Understanding partitioning in PySpark

* Partition is a logical division of a large disstributed data set. 
`minPartitions` - the minimum number of partitions
* `parallelize()` method.
```python
numRDD = sc.parallelize(range(10), minPartitions = 6)
```
* `textFile()`
```python
fileRDD = sc.textFile('README.md', minPartitions = 6)
```

*Get number of partitions*
```python
fileRDD.getNumPartitions()
```


## Basic RDD transformations 

* RDD transformations
    * Transformations follow Lazy evaluation
    * Basic RDD transformations
        * `map(), filter(), flatMap(), union()`

            * map() transformation applies a function to all elements in the RDD
            ```python
            RDD = sc.parallelize([1,2,3,4])
            RDD_map = RDD.map(lambda x: x*x)
            ```
            * filter() return a new RDD with only the elements that pass the condition
            ```python
            RDD = sc.parallelize([1,2,3,4])
            RDD_filter = RDD.map(lambda x: x>2)
            ```
            * flatMap() returns multiple vals for each element in the original RDD
            `["Hello world", "test"]` -> `flatMap(x: x.split("")` -> `["Hello", "world", "test"]`
            * union() combine to RDD

            ```python
            inputRDD = sc.textFile('logs.txt')
            errorRDD = inputRDD.filter(lambda x: "error" in x.split())
            warningsRDD = inputRDD.filter(lambda x: "warnings" in x.split())
            combined = errorRDD.union(warningsRDD)
            ```
* RDD actions
    * Operation return a value after running a computation on the RDD
    * Basic RDD actions 
        * collect()
        * take()
        * first()
        * count()

        * collect() and take() Actions
            * collect() return all the elements of the datasets as an array
            * take(N) return first N elements
            * first() - first element
            * count() - number of elements

## Working with pair RDDs

* Example of paired RDD transformations
    * reduceByKey(func) : Combine vals with same key

    * groupByKey() : Group vals with the same key
    * sortByKey(): Returned RDD sorted by the key

##### reduceByKey(func)
* `reduceByKey()` transformation combines values with the same key
* It runs parallel operations for each key in dataset
* It is a transformation and not action 
```python
# reduceByKey()

regularRDD = sc.parallelize([("Messi",23), ("Ronaldo", 34), ("Neymar", 22), ("Messi", 24)])
pairRDD_reducebykey = regularRDD.reduceByKey(lambda x,y : x + y)
pairRDD_reducebykey.collect()

# [("Messi",47), ("Ronaldo", 34), ("Neymar", 22)
```

## Advanced actions

*reduce() action*

* reduce(func) action is used for aggregating the elements of a regular RDD
* func should be commutative (changing the order of the operands does not change the result) and associative

*saveAsTextFile() action*

* saves RDD into a text file inside a directory with each partition as a separate file
`RDD.saveAsTextFile("tempFile")`
* coalesce() method can be used to save RDD as a single text file
`RDD.coalesce(1).saveAsTextFile("tempFile")`

*Action operations on pair RDDs*

* RDDS actions available for PySpark pair RDDs
* Pair RDD actions leverage the key-value data
* Few examples of pair RDD actions include
    * countByKey() counts the number of elements for each key
    * collectAsMap() return the k-v pairs in the RDD as dict


## Word count with RDD (PySpark)
```python
# Display the first 10 words and their frequencies
for word in resultRDD.take(10):
	print(word)

# Swap the keys and values 
resultRDD_swap = resultRDD.map(lambda x: (x[1], x[0]))

# Sort the keys in descending order
resultRDD_swap_sort = resultRDD_swap.sortByKey(ascending=False)

# Show the top 10 most frequent words and their frequencies
for word in resultRDD_swap_sort.take(10):
	print("{} has {} counts". format(word[1], word[0]))
```