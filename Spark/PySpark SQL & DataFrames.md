# PySpark SQL & DataFrames
Spark SQL allows you to use DataFrames in Python.

* PySpark SQL is a Spark lib for structured data
* DF spark is immutable distributed collections of data with named columns
* Processing both structured (relational DB) and semi-structured data (JSON)
* API available in Python, Scala, Java
* DF support SQL queries or expression methods

## SparkSession - entry point for DF API

* SparkContext main entry point for creating RDDs
* SparkSession provides a single point of entru to interact with Spark DF
* Used to create DF, register DF, execture SQL queries

## Creating DF

* Two method
    1. From existing RDDs using SparkSession createDataFrame()
    2. From data sources (CSV, Json, TXT) using SparkSession read method
* Schema control the data and help DF to optim queries
* Schema provides info about column name, type of data in the column, etc.

## Interacting with DF

* DF operations : Transformations and Actions
* DF transformations :
    * select(), filter(), groupby(), orderby(), dropDuplicates(), withColumnRenamed()

## DF api vs SQL queries

* DF API provides a programmatic domain-specific language (DSL) for data
* DF transformations and actions are easier to construct programmaticly
* SQL queries can be easier to understand and portable

*Executing SQL queries*

* The SparkSession `sql()` execute SQL query
* Method take SQL statement and return DF
* Can't directly create SQL query. Need to create temp table
```python
df.createOrReplaceTempView("table1")
df2 = spark.sql("SELECT field1, field2 FROM table1)
df2.collect()
```
*SQL query to extract data*
```python
test_df.createOrReplaceTempView("test_table")
query = '''SELECT Product_ID FROM test_table'''
test_product_df = spark.sql(query)
test_product_df.show(5)
```
## Data Visualization

* Pyspark_dist_explore provide quick insight into DF
* Three func available
    * hist()
    * distplot()
    * pandas_histogram()