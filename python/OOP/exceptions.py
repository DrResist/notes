# Try except finally for check exception

# Custom exception

class BalanceError(Exception):
    pass


class Customer:
    def __init__(self, name, balance):
        if balance < 0:
            raise BalanceError("Balance as to be non-negative")
        else:
            self.name, self.balance = name, balance
