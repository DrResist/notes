# Designing for inheritance and polymorphism

# polymorphism - using a unified interface to operate on objects of diff classes

# Managing data access 

"""
All classes data is public

Restricting access

Naming conventions

* Start with a single _ mean internal 
* Not a part of the public API
* As a cluss user: "don't touch this"

* __ mean private
* Not inherited

Use @property to customize access
Overriding __getattr__() and __setattr__()
"""

# Changing attributes


class Employee:
    def set_name(self, name):
        self.name = name
    def set_salary(self, salary):
        self.salary = salary
    def give_raies(self, amount):
        self.salary = self.salary + amount

    def __init__(self, name, salary):
        self.name, self.salary = name, salary


class Employer:
    def __init__(self, name, new_salary):

    @property
    def salary(self):
        return self._salary

    @salary.setter
    def salary(self, new_salary):

        if new_salary < 0:
            raise ValueError("Invalid salary")

        self._salary = new_salary


class Customer:
    def __init__(self, name, new_bal):
        self.name = name
        if new_bal < 0:
           raise ValueError("Invalid balance!")
        self._balance = new_bal  

    # Add a decorated balance() method returning _balance        
    @property
    def balance(self):
        return self._balance
     
    # Add a setter balance() method
    @balance.setter
    def balance(self, new_bal):
        # Validate the parameter value
        
        if new_bal < 0:
            raise ValueError("Invalid balance")
            
        # Print "Setter method is called"
        print("Setter method is called")