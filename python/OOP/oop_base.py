class SavingAccount(BankAccount):

	# Constructor spec for SavingsAccount with an add parameter
	def __init__(self, balance, interest_rate):
		# Call parent constr using ClassName.__init__()
		BankAccount.__init__(self, balance)
		# self is SavingsAccount but also a BankAccount
		self.interest_rate = interest_rate

	def compute_interest(self,n_periods = 1)
		return self.balance * ((1+ self.interest_rate) ** n_periods - 1)


class CheckingAccount(BankAccount):
	def __init__(self,balance, limit):
		BankAccount.__init__(self, content)
		self.limit = limit

	def deposit(self, amount):
		self.balance += amount 

	def withdraw(self, amount, fee=0):

		if fee <= self.limit:
			BankAccount.withdraw(self, amount - fee)
		else:
			BankAccount.withdraw(self, amount - self.limit)

"""
Can change the signature (add parameters)
Use Parent.method(self, args ...) to call a method from the parent class
"""
# Original from_str method for reference:
#     @classmethod
#     def from_str(cls, datestr):
#         year, month, day = map(int, datestr.split("-"))
#         return cls(year, month, day)
  
# Define an EvenBetterDate class and customize from_str
class EvenBetterDate(BetterDate):
    @classmethod
    def from_str(cls, datestr, format="YYYY-MM-DD"):
        if format == "YYYY-MM-DD":
            return BetterDate.from_str(datestr)
        elif format == "DD-MM-YYYY":
            day, month, year = map(int, datestr.split("-"))
            return cls(year, month, day)

# This code should run with no errors
ebd_str = EvenBetterDate.from_str('02-12-2019', format='DD-MM-YYYY')
print(ebd_str.year)
ebd_dt = EvenBetterDate.from_datetime(datetime.today())
print(ebd_dt.year)


# Import pandas as pd
import pandas as pd

# Define LoggedDF inherited from pd.DataFrame and add the constructor
class LoggedDF(pd.DataFrame):
  
  def __init__(self, *args, **kwargs):
    pd.DataFrame.__init__(self, *args, **kwargs)
    self.created_at = datetime.today()
    
  def to_csv(self, *args, **kwargs):
    # Copy self to a temporary DataFrame
    temp = self.copy()
    
    # Create a new column filled with self.created at
    temp["created_at"] = self.created_at
    
    # Call pd.DataFrame.to_csv on temp with *args and **kwargs
    pd.DataFrame.to_csv(temp, *args, **kwargs)

