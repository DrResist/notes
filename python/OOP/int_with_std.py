# /bin/env/python3

'''
3 module in OOP 
'''

# Operator overloading: comprasion. Object equality

class Customer:
    def __init__(self, name, balance, id):
        self.name, self.balance = name, balance
        self.id = id

cust1 = Customer("Mat Az", 3000)
cust2 = Customer("Mat Az", 3000)

cust1 == cust2 # False


class Customer:
    def __init__(self, name, balance, id):
        self.name, self.balance = name, balance
        self.id = id

    # Will be called when == is used
    def __eq__(self, other):
        # Diag print 
        print("__eq__() is called")

        # Returns True if all attr  match
        return (self.id == other.id) and \
                (self.name == other.name)

'''
== - __eq__
!= - __ne__
>= - __ge__
<= - __le__
> - __gt__
< - __lt__

__hash__ to use obj as dict keys and in sets

__str__() for end user (string representation)
__repr__() formal for devs

'''
# Implementation 


class Customer:
    def __init__(self, name, balance):
        self.name, self.balance = name, balance
    
    def __str__(self):
        cust_str = """
        Customer: {name}
        balance: {balance}
        """.format(name = self.name, \
                   balance = self.balance)
        return cust_str

cust = Customer("Mar azar", 3000)

# Will impl call __str__
print(cust)

# Customer:
#   name: Mar azar
#   balance: 3000


