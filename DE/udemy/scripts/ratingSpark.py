#!/usr/bin/env python3

from pyspark import SparkConf, SparkContext

# Create py dict for converting movieID to movieName

def loadMovieNames():
    movieNames = {}
    with open("ml-100k/u.item") as f:
        for line in f:
            fields = line.split("|")
            movieNames[int(fields[0])] = fields[1]
    return movieNames

# Each line of u.data and convert to (movieID, (rating, 1.0))

def parseInput(line: str) -> tuple:
    fields = line.split()
    return (int(fields[1]), (float(fields[2]), 1.0))

if __name__ == "__main__":
    # main script create SparkContext
    conf = SparkConf().setAppName("WorstMovies")
    sc = SparkContext(conf = conf)

    # Load movieID -> movie name table
    movieNames = loadMovieNames()

    # load raw u.data
    lines = sc.textFile("hdfs:///user/maria_dev/ml-100k/u.data")

    # Convert to movieID (rating,1)
    movieRatings = lines.map(parseInput)

    # Reduce to (movieID, (sumOfRatings, totalRatings))
    ratingTotalsAndCount = movieRatings.reduceByKey(
        lambda movie1, movie2: (movie1[0] + movie2[0])
    )

    # Map to (movieID, avgRatings)
    averageRatins = ratingTotalsAndCount.mapValues(lambda totalAndCount : totalAndCount[0] / totalAndCount[1])

    sortedMovies = averageRatins.sortBy(lambda x: x[1])

    # Take top 10

    results = sortedMovies.take(10)

    # print
    for result in results:
        print(movieNames[result[0]], result[1])
