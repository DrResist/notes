#!/usr/bin/env python3

from pyspark.sql import SparkSession
from pyspark.sql import Row
from pyspark.sql import functions

def parseInput(line):
    fields = line.split('|')
    return Row(
        user_id = int(fields[0]),
        age = int(fields[1]),
        gender = fields[2],
        occupation = fields[3],
        zip = fields[4])
if __name__ == "__main__":
    # Create SparkSession
    spark = SparkSession.appName("MongoDBIntegraion").getOrCreate()

    # Get raw data
    lines = spark.sparkContext.textFile("hdfs:///user/maria_dev/ml-100k/u.user")

    # Convert -> RDD of Row obj
    users = lines.map(parseInput)

    # Convert RDD -> DF
    usersDataset = spark.createDataFrame(users)

    # Write it into MongoDB
    usersDataset.write\
        .format("com.mongodb.spark.sql.DefaultSource")\
        .option("uri", "mongodb://127.0.0.1/movielens.users")\
        .mode("append")\
        .save()

    # Read it back from MongoDB into a new DF

    readUsers = spark.read\
        .format("com.mongodb.spark.sql.DefaultSource")\
        .option("uri", "mongodb://127.0.0.1/movielens.users")\
        .load()

    readUsers.createOrReplaceTempView("users")

    sqlDF = spark.sql("SELECT * FROM users WHERE age < 20")
    sqlDf.show()

    # Stop
    spark.stop()
