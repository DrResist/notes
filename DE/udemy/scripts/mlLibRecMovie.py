#!/usr/bin/env python3

from pyspark.sql import SparkSession
from pyspark.ml.recommendation import ALS
from pyspark.sql import Row
from pyspark.sql.functions import lit

# Load movie ID - name

def loadMovieNames():
    movieNames = {}
    with open("ml-100k/u.item") as f:
        for line in f:
            fields = line.split('|')
            movieNames[int(fields[0])] = fields[1].decode('ascii', 'ignore')
    return movieNames

# Convert u.data into (userID, movieID, rating)
def parseInput(line) -> pyspark.sql[Row]:
    fields = line.value.split()
    return Row(userID = int(fields[0]), movieID = int(fields[1]), rating = float(fields[2]))

if __name__ == "__main__":
    # Create Spark Session

    spark = SparkSession.builder.appName("MovieRecs").getOrCreate()

    # Load movieID -> name dict

    movieNames = loadMovieNames()

    # Get raw data

    lines = spark.read.text("hdfs:///user/maria_dev/ml-100k/u.data").rdd

    # Convert -> RDD of Row
    ratingRDD = lines.map(parseInput)

    # Convert -> DF and cache
    ratings = spark.createDataFrame(ratingRDD).cache()

    # Create an ASL collaborative filtering model from data set

    als = ALS(maxIter=5, regParam=0.01,userCol="userID",itemCol="movieID",ratingCol="rating")
    model = als.fit(ratings)

    # print ratings from user 0
    print("\nRatings for user ID 0:")
    userRatings = ratings.filter("userID = 0")
    for rating in userRatings.collect():
        print(movieNames[rating['movieID']], rating['rating'])

    # top 20 recs

    print("\nTop 20 recommendations:")

    # Find movies rated more than 100 times
    ratingCounts = ratings.groupBy("movieID").count().filter("count > 100")
    # Construct test DF for user 0 with every movie rated > 100
    popularMovies = ratingCounts.select("movieID").withColumn("userID", lit(0))

    # Run model on list of popular movies for user ID 0
    recommendations = model.transform(popularMovies)

    # Top 20
    topRecommendations = recommendations.sort(recommendations.prediction.desc()).take(20)

    for recs in topRecommendations:
        print(movieNames[recommendations["movieID"]], recommendations["prediction"])

    spark.stop()
