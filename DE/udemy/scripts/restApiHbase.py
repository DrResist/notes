#!/usr/bin/env python3

from starbase import Connection

c = Connection("127.0.0.1", "8000")

ratings = c.table('ratings')

if (ratings.exists()):
    print("Dropping exist rating table\n")
    ratings.drop()

ratings.create('rating')

print("Parsink ml-100k ratins data ...\n")
ratingFile = open("/home/drresist/Yandex.Disk/Notes/notes/DE/udemy/u.data", "r")

batch = ratings.batch()

for line in ratingFile:
    (userID, movieID, rating, timestamp) = line.split()
    batch.update(userID, {'rating': {movieID:rating}})

ratingFile.close()

print("Commiting ratings data to Hbase via REST service\n")
batch.commit(finalize=True)

# Quering data

print("Ratings for user ID 1:\n")
print(ratings.fetch("1"))
