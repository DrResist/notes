#!/usr/bin/env python3

from pyspark.sql import SparkSession
from pyspark.sql import Row
from pyspark.sql import functions

def loadMovieNames():
    movieNames ={}
    with open("ml-100k/u.item") as f:
        for line in f:
            fields = line.split('|')
            movieNames[int(fields[0])] = fields[1]
    return movieNames

def parseInput(line):
    fields = line.split()
    return Row(movieID = int(fields[1]), rating = float(fields[2]))

if __name__ == "__main__":
    # Create SparkSession
    # getOrCreate - if task failure, we can get previous session and don't create new
    spark = SparkSession.builder.appName("PopularMovies").getOrCreate()

    # Load up movieID -> name
    movieNames = loadMovieNames()

    # Get raw data
    lines = spark.sparkContext.textFile("hdfs:///user/maria_dev/ml-100k/u.data")

    # Convert to a RDD of Row (movieID, rating)
    movies = lines.map(parseInput)

    #Convert to a DF
    movieDataset = spark.createDataFrame(movies)

    # avg rating for each movieID
    averageRatings = movieDataset.groupBy("movieID").avg("rating")

    # count
    counts = movieDataset.groupBy("movieID").count()

    # Join two together (movieID, avg(rating), count columns
    averagesAndCounts = counts.join(averageRatings, "movieID")

    # Top 10
    topTen = averagesAndCounts.orderBy("avg(rating)").take(10)

    # Print
    for movie in topTen:
        print(movieNames[movie[0]], movie[1], movie[2])

    # Stop session
    spark.stop()
