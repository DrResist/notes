#+TITLE: Programming Hadoop With Spark
#+AUTHOR: Maksim Fesenko

* Why Spark?
** What is spark?
Fast and general engine for large-scale data processing.
** It's scalable
Consist of Driver Program - SparkContext. Next level is Cluster Manager (Spark, YARN), that split task throug executor's (Cache, tasks)
Cache in RAM for faster work
** It's faster
+ 100x then MapReduce in memory
+ DAG engine (directed acyclic graph) optimizes workflows
** Not hard
+ Code python, Java, Scala
+ Built around RDD (Resilient Distributed Dataset)

** Components
+ Spark Streaming for RT work
+ Spark SQL - SQL interface
+ MLLib for ML - machine learning
+ GraphX for graph-based DB
In fundament - Spark Core
** Scala code in Spark looks a lot like python
#+begin_src python
nums = sc.parallelize([1,2,3,4])
squared = nums.map(lambda x: x*x).collect()
#+end_src
For scala
#+begin_src scala
val nums = sc.parallelize(List(1,2,3,4))
val squared = nums.map(x => x*x).collect()
#+end_src

* RDDs
+ Resilient
+ Distributed
+ Dataset
** Creating RDD's
*** SparkContext
+ Created by driver program
+ Is responsible for making RDD's resilient and distributed
+ Creates RDD's
+ Spark Shell creates a "sc" object

Creating
#+begin_src python
nums = parallelize([1,2,3,4])

sc.textFile("file://...") or ("s3n://") or ("hdfs://")

hiveCtx = HiveContext(sc)
rows = hiveCtx.sql("SELECT name, age FROM users")
#+end_src
Also can created from:
+ JDBC
+ Cassandra
+ HBase
+ ElasticSearch
+ etc
** Transforming RDD's
+ map - 1-1 relation
+ flatmap - for multiple rows (any relations)
+ filter
+ distinct
+ sample
+ union, intersection, substract, cartesian
*** Map example

#+begin_src python
rdd = sc.parallelize([1,2,3,4])
squaredRDD = rdd.map(lambda x: x*x)
#+end_src
*** Lazy evaluation
Nothing happens in your driver program until an action is called
** Execution spark script in HDP

*** Spark log level settings
Go to Ambari as admin -> Services -> Spark(2) -> Configs -> Advanced spark2-log4j-properties
#+begin_src conf
log4j.rootCategory=INFO, console
# Change to ERROR
log4j.rootCategory=ERROR, console

#+end_src

** Execution spark script on server
#+begin_src bash
spark-submit LowestRatedMovieSpark.py
#+end_src
#+result
SPARK_MAJOR_VERSION is set to 2, using Spark2
('3 Ninjas: High Noon At Mega Mountain (1998)', 1.0)
('Beyond Bedlam (1993)', 1.0)
('Power 98 (1995)', 1.0)
('Bloody Child, The (1996)', 1.0)
('Amityville: Dollhouse (1996)', 1.0)
('Babyfever (1994)', 1.0)
('Homage (1995)', 1.0)
('Somebody to Love (1994)', 1.0)
('Crude Oasis, The (1995)', 1.0)
('Every Other Weekend (1990)', 1.0)

* Datasets and Spark2
SparkSQL
+ Extend RDD to a DF object
+ DF:
  + Contain raw object
  + run SQL queries
  + Has a schema (more efficient storage)
  + R/W to JSON, Hive, parquet
  + Communicates with JDBC/ODBC, Tableu

** Using SparkSQL
#+begin_src python
from pyspark.sql import SQLContext, Row
hiveContext = HiveContext(sc)
inputData = spark.read.json(dataFile)
etc.
#+end_src
** Dataset
+ DF is DataSet of Row obj in Spark 2
+ DS can wrap known, typed data too.
** Shell access
+ Spark SQL JDBC/ODBC
+ Start sbin/start-thrftserver.sh
+ Listen on 10000 by default
+ Connect using bin/beeline -u jdbc:hive2://localhost:10000
** UDF
#+begin_src python
from pyspark.sql.types import IntegerType

hiveCtx.registerFunction("square", lambda x: x*x, IntegerType())
df = hiveCtx.sql("SELECT square("someNumericField") FROM tableName")
#+end_src
* MLLib in Spark
