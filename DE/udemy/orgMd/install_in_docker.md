## Install pip

`docker exec -it yum install python-pip`

## MRjob

`docker exec -it pip install mrjob==0.5.11`

## NANO

`docker exec -it yum install nano`

## Test files and script

`wget http://media.sundog-soft.com/hadoop/ml-100k/u.data`
`wget http://media.sundog-soft.com/hadoop/RatingsBreakdown.py`

## Run locally

`python RatingsBreakdown.py u.data`

## Run with hadoop

python RatingsBreakdown.py -r hadoop --hadoop-streaming-jar /usr/hdp/current/hadoop-mapreduce-client/hadoop-streaming.jar u.data