#+TITLE: Query Engines
#+Author: Maksim Fesenko

* Overview
+ Drill
+ Phoenix
+ Presto
+ Zeppelin
+ HUE
* Apache DRILL
** What is?
+ SQL query engine for a var of non-relational DB and DF
    - Hive, MongoDB
    - Json, Parquet, S3, Azue
+ Based on Google's Dremel
+ It is real SQL, Not sql-like
+ Has ODBC/JDBC driver. Other tool can connect to it like RDMBS
+ Still non-relational DB under hood
+ Allows SQL analysis of disparte ds without having to transform and load it first
  - Internally data is represented as JSON ans so has no fixed schemas
+ Can do joins across different DB tech
** Setting Drill
1. In Hive create DB and upload Table
   #+begin_src sql
CREATE DATABASE movielens;
   #+end_src
2. Import data to Mongo
   #+begin_src shell
spark-submit --packages org.mongodb.spark:mongo-spark-connector_2.11:2.3.0 MongoSpark.py
   #+end_src
3. Drill not part of HDP. Version 1.12
   #+begin_src shell
wget http://archive.apache.org/dist/drill/drill-1.12.0/apache-drill-1.12.0.tar.gz
tar -xvf ...
cd ...
bin/drillbit.sh start -Ddrill.exec.http.port=8765
   #+end_src
4. 127.0.0.1:8765 drill interface. Enable Hive and Mongo in Storage Tab and change config of hive:
   #+begin_src config
    "hive.metastore.uris": "thrift://localhost:9083",
   #+end_src
** Query in Drill
Standart SQL language query

To stop Drill
#+begin_src shell
/bin/drillbit.sh stop
#+end_src
