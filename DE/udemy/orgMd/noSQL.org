#+TITLE: No Sql
#+AUTHOR: Maksim Fesenko

* NoSQL (when RDBMS don't cut it)
** Random access to planet-size data

*** Way to work with BIG data (RDMBS)
+ Denormalization
+ Caching layers
+ Master/Slave setups
+ Sharding
+ Materialized views
+ Removig stored procedures
*** Architecture (sample)
Client&Request router with many shard (N) and shard backup's (N)
** Right tool for job's
+ For analytics queries - Hive, Pig, Spark, etc.
+ For exporting data MySQL is plenty fast for most app's
+ If neeed giant scale - export data to non-relational DB for fast and scalable serving of tat data to web app's, etc.
** Sample app architecture
Customers -> Internet -> Web servers -> MongoDB <- SparkStreaming | Hadoop YARN | HDFS <- Data Sources

Every block represent many machines
* HBASE
Non-relational scalable DBC built on HDFS
+ Don't have SQL language. Using CRUD API's (Create Read Update Delete)
+ Architecture - Many region server (range of keys) with auto-sharding. Based on HDFS.
** HBase data model
+ Fast acess to any given ROW
+ A ROW is referensed by a unique KEY
+ Each ROW has small number of COLUMN FAMILIES
+ A COLUMN FAMILIES may contain arbitrary COLUMNS
+ Can have a very large numver of COLUMNS in a COLUMN FAMILY
+ Each CELL can have many VERSIONS with given timestamps
+ Sparse data is A-OK - missing columns in a row consume no storage
*** Example: One Row of a web table
| Key         | Contents column familt       | Anchor column family                                 |
|-------------+------------------------------+------------------------------------------------------|
| com.cnn.www | Contents: <html><head>CNN... | Anchor: cnnsi.com "CNN" Anchor: my.look.ca "CNN.com" |
** Ways to access HBase
+ HBase shell
+ Java API
+ Spark, Hive, Pig
+ REST Service
+ Thrift service
+ Avro service
* Creating table
Using python and REST interface
| UserID  | Column family: rating |            |            |
|---------+-----------------------+------------+------------|
|         | Rating: 50            | Rating: 33 | Rating:223 |
| User ID | 1                     |          5 |          5 |
|         |                       |            |            |

Python client -> REST svc -> HBase |  HDFS

REST svc on 8000 port
To UP rest service:
#+begin_src shell
su root
/usr/hdp/current/hbase-master/bin/hbase-daemon.sh start rest -p 8000 --infoport 8001
#+end_src

** Python script to work with REST
