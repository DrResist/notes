# Map Reduce fund 

**MapReduce**:
* Distributes the processing of data on cluster
* Divides data up into partitions that are MAPPED (transformed) and REDUCED (aggregated) by mapper and reducer function
* Resilient to failure - app master monitors mappers and reducers on each partition

## Example: 

Table with : User ID | Movie ID | RATING | TIMESTAMP

1. Mapper. We map user ID with Movie ID like:
```
196:242 186:302 196:377 244:51 166:346 186:274 186:265
```
2. Shuffle and sort. 
```
166:346 186:302,274,265 196:242,377 244:51
```
3. Reducer (len(movie))
```
166:1 186:3 196:2 244:1
```

## MapReduce on cluster


Mapper chop data on chunks that reduced on clusters. 

### What's happening

Client Node - Yarn - NodeManager (MapReduce, Application master) 

## mapreduce written

* Written is natievly Java
* STREAMING allows interfacing to other languages (Python)

## Handling Failure

* App master monitor worker tasks for errors or hanging
  * Restarts as needed
  * Prefere another node
* What if app master goes down?
  * YARN try to restart it
* What if an entire Node goes down?
  * This could be the App master
  * Resource manager will try to restart it
* What if Resource manager down?
  * Can setup "high availability" (HA) using ZooKeeper to have a hot standby
  
## Making mapreduce problem

* MAP each input line to (raiting, 1)
* REDUCE each ratings with the sum of all the 1's

```python
def mapper_get_ratings(self, _, line):
    (userID, movieID, rating, timestamp) = line.split('\t')
    yield rating, 1
def reducer_count_ratings(self, key, values):
    yield key, sum(values)
```

All together:
```python
from mrjob.job import MRJob
from mrjob.step import MRStep

class RatingsBreakdown(MrJob):
  def steps(self):
    return [
      MRStep(mapper = self.mapper_get_ratings,
             reducer = self.reducer_count_ratings)
    ]
  def mapper_get_ratings(self, _, line):
      (userID, movieID, rating, timestamp) = line.split('\t')
      yield rating, 1
def reducer_count_ratings(self, key, values):
      yield key, sum(values)
if __name__ == '__main__':
    RatingsBreakdown.run()
```
