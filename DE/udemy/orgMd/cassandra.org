#+TITLE: Cassandra
#+Author: Maksim Fesenko

* Cassandra

NoSQL with a twist.
+ Unlike HBase there is no master node at all - every node runs exactly the same software and performs the same functions
+ Data model is similar to BigTable / HBase
+ Non-raltional, but has a limited SQL query language as its interface
** Cassandra's design choices
+ CAP theorem says can have only 2 out of 3:
  + Consistency
  + Availability
  + Partition-tolerance
+ Casandra favors availability over consistency
  + It is "eventually consistent"
  + Can specify consistency requirements as a part of request. "Tunable consistency"
** Cassandra arch
  + Ring scheme - all node's in create ring
** Cassandra and cluster
+ Cassandra great for fast access to rows of information
+ Best of both worlds - replicate Cassandra to a another ring that is used for analytics and Spark
** CQL (cassandra query language)
+ Cass Api is CQL
+ SQL is like SQL with limit:
  + No Joins (data de-normalized)
  + Non-relational
  + All queries must be on some PK
+ CQLSH (CQL shell) CLI to create table, etc
+ All tables myst be in keyspaces - are like databases
** Cassandra and Spark
+ DataStax offers a Spark-Cassandra connector
+ allow RW Cassandra as a DF
+ Use case:
    + Use spark for analytics on data stored in Cassanda
    + Use spark to transform data and store it into Cassandra for transactional uses
* Cassandra installation
Need Python 2.7 (In HDP 2.6.5 allready)
#+begin_src shell
cd /etc/yum.repos.d/
vi datastax.repo
# In .repo
[datastax]
name = DataStax Repo for Apache Cassandra
baseurl = https://rpm.datastax.com/community
enabled = 1
gpgcheck = 0
# In shell
yum install cassandra
# Start cassandra service
services cassandra start
# To run cassandra
cqlsh
# If problem with version
cqlsh --cqlversion="3.4.0"
#+end_src
Creating keyspace (database)
#+begin_src sql
create keyspace movielens WITH replication = {'class':'SimpleStrategy','replication_factor':'1'} AND durable_writes = true

CREATE TABLE users (user_id int, age int, gender text, occupation text, zip text, PRIMARY KEY (user_id));
#+end_src

* From Spark to Cassandra
To run
#+begin_src shell
spark-submit --packages datastax:spark-cassandra-connector:2.0.0-M2-s_2.11 CassandraSpark.py
#+end_src
#+begin_src python
from pyspark.sql import SparkSession
from pyspark.sql import Row
from pyspark.sql import functions

def parseInput(line):
    fields = line.split('|')
    return Row(user_id = int(fields[0]), age = int(fields[1]), gender = fields[2], occupation = fields[3], zip = fields[4])

if __name__ == "__main__":
    # Create a SparkSession
    spark = SparkSession.builder.appName("CassandraIntegration").config("spark.cassandra.connection.host", "127.0.0.1").getOrCreate()

    # Get the raw data
    lines = spark.sparkContext.textFile("hdfs:///user/maria_dev/ml-100k/u.user")
    # Convert it to a RDD of Row objects with (userID, age, gender, occupation, zip)
    users = lines.map(parseInput)
    # Convert that to a DataFrame
    usersDataset = spark.createDataFrame(users)

    # Write it into Cassandra
    usersDataset.write\
        .format("org.apache.spark.sql.cassandra")\
        .mode('append')\
        .options(table="users", keyspace="movielens")\
        .save()

    # Read it back from Cassandra into a new Dataframe
    readUsers = spark.read\
    .format("org.apache.spark.sql.cassandra")\
    .options(table="users", keyspace="movielens")\
    .load()

    readUsers.createOrReplaceTempView("users")

    sqlDF = spark.sql("SELECT * FROM users WHERE age < 20")
    sqlDF.show()

    # Stop the session
    spark.stop()
#+end_src
