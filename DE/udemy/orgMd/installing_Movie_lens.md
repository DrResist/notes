# Installing movielens



Link: https://grouplens.org/datasets/movielens/

When upload - it will be replicated 

## Install movielens with CLI 

To connect:

`ssh maria_dev@127.0.0.1 -p 2222`

To use CLI commands:
`hadoop fs -ls`

To Copy from local system into HDFS:
`hadoop fs -copyFromLocal u.data ml-100k/u.data`
