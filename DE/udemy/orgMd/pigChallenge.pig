ratings = LOAD '/user/maria_dev/ml-100k/u.data' AS (userID:int, movieID:int, rating:int, ratingTime:int);
metadata = LOAD '/user/maria_dev/ml-100k/u.item' USING PigStorage('|') AS (movieID:int, movieTitle:chararray, releaseDate:chararray, videoRelease:chararray, imbdLink:chararray);
nameLookup = FOREACH metadata GENERATE movieID, movieTitle, ToUnixTime(ToDate(releaseDate, 'dd-MM-yyy')) AS releaseTime;

-- group by rating
groupedRatings = GROUP ratings BY movieID;

-- create avg rating and counter of rating
avgRatings = FOREACH groupedRatings GENERATE group AS movieID,
 AVG(ratings.rating) AS avgRating,
 COUNT(ratings.rating) AS numRatings;

-- filter data "less then 2.0"

badFilmes = FILTER avgRatings BY avgRating < 2.0;

-- Join all and sort

namedBadMovies = JOIN badFilmes BY movieID, nameLookup BY movieID;

finalData = FOREACH namedBadMovies GENERATE nameLookup::movieTitle AS movieName,
    badFilmes::avgRating AS avgRating, badFilmes::numRatings AS numRatings;

finalResultsSorted = ORDER finalData BY numRatings DESC;

DUMP finalResultsSorted;
